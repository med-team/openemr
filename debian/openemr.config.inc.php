<?php
// OpenEMR Integartion

// If OpenEMRphMyAdmin cookie we provide the basic configuration
// All crednetials are handled by interface/main/left_nav.php
if ( isset($_COOKIE['OpenEMRphpMyAdmin']) ) {
    $i=$i++;
    // Single signon server configuration for using phpMyAdmin inside of OpenEMR
    // We use a bridge session defined in globals.php for path /openemr/phpmyadmin that has
    // the required information in it for login
    $cfg['Servers'][$i]['extension'] = 'mysqli';
    $cfg['Servers'][$i]['auth_type'] = 'signon';
    $cfg['Servers'][$i]['SignonSession'] = 'OpenEMRphpMyAdmin';
    $cfg['Servers'][$i]['SignonURL'] = '/openemr';
    $cfg['Servers'][$i]['only_db'] = 'openemr';
    $cfg['ServerDefault'] = $i;
    $cfg['AllowThirdPartyFraming'] = TRUE;
    $cfg['ShowCreateDb'] = FALSE;
    $cfg['ShowPhpInfo'] = TRUE;
    $cfg['Confirm'] = TRUE;
    $cfg['Error_Handler']['display'] = TRUE;
} else {
    error_log("oer:  ");
}

?>
