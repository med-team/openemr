# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: openemr\n"
"Report-Msgid-Bugs-To: openemr@packages.debian.org\n"
"POT-Creation-Date: 2014-08-08 21:41-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:1001 ../templates:2001 ../templates:3001 ../templates:4001
#: ../templates:5001 ../templates:6001
msgid "MySQL Root Password:"
msgstr ""

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:1001 ../templates:2001 ../templates:3001
msgid ""
"If you enter the correct MySQL root password, then OpenEMR will be installed "
"and automatically configured."
msgstr ""

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:1001 ../templates:2001 ../templates:3001
msgid ""
"If you do not enter the correct MySQL root password, then OpenEMR will be "
"installed, but will not be automatically configured."
msgstr ""

#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:1001 ../templates:4001
msgid ""
"You will be given 3 attempts to answer this question. This is your first "
"attempt."
msgstr ""

#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:2001 ../templates:5001
msgid ""
"You will be given 3 attempts to answer this question. This is your second "
"attempt."
msgstr ""

#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:3001 ../templates:6001
msgid ""
"You will be given 3 attempts to answer this question. This is your last "
"attempt."
msgstr ""

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:4001 ../templates:5001 ../templates:6001
msgid ""
"In order to remove this package, you need to enter the correct MySQL root "
"password."
msgstr ""

#. Type: select
#. Choices
#: ../templates:7001
msgid "yes, no"
msgstr ""

#. Type: select
#. Description
#: ../templates:7002
msgid "Attempt OpenEMR Upgrade?"
msgstr ""

#. Type: select
#. Description
#: ../templates:7002
msgid ""
"Found a version of OpenEMR at /var/www/openemr. Do you want to attempt an "
"upgrade (ensure you have a backup before proceeding)?"
msgstr ""

#. Type: select
#. Choices
#: ../templates:8001
msgid "no, yes"
msgstr ""

#. Type: select
#. Description
#: ../templates:8002
msgid "Confirm OpenEMR Removal?"
msgstr ""

#. Type: select
#. Description
#: ../templates:8002
msgid ""
"Are you sure you really want to remove the OpenEMR package? All OpenEMR data "
"will be removed if you remove this package!"
msgstr ""

#. Type: note
#. Description
#. Type: note
#. Description
#. Type: note
#. Description
#. Type: note
#. Description
#. Type: note
#. Description
#. Type: note
#. Description
#: ../templates:9001 ../templates:10001 ../templates:11001 ../templates:12001
#: ../templates:13001 ../templates:14001
msgid "NOTICE"
msgstr ""

#. Type: note
#. Description
#: ../templates:9001
msgid "You have chosen to not install the OpenEMR package."
msgstr ""

#. Type: note
#. Description
#: ../templates:10001
msgid ""
"OpenEMR is going to be installed, however it will not be automatically "
"configured (because you did not provide the MySQL root password)."
msgstr ""

#. Type: note
#. Description
#: ../templates:11001
msgid ""
"OpenEMR is going to be installed, however it will not be automatically "
"configured (because a openemr MySQL database already exists)."
msgstr ""

#. Type: note
#. Description
#: ../templates:12001
msgid ""
"OpenEMR is going to be installed, however it will not be automatically "
"configured (because a openemr MySQL user already exists)."
msgstr ""

#. Type: note
#. Description
#: ../templates:13001
msgid ""
"Entries have been added to your apache configuration to secure directories "
"with patient information. Placed backup of your original apache "
"configuration file to /etc/apache2/httpd.conf.BAK ."
msgstr ""

#. Type: note
#. Description
#: ../templates:14001
msgid ""
"To ensure compatibility with php, settings have been modified in your php "
"configuration file, and a backup of your original php configuration file has "
"been placed at /etc/php5/apache2/php.ini.BAK . For more information on the "
"php settings that have been modified, please see the log file for more "
"information after the installation is complete, /var/log/openemr/install ."
msgstr ""

#. Type: note
#. Description
#. Type: note
#. Description
#. Type: note
#. Description
#: ../templates:15001 ../templates:16001 ../templates:17001
msgid "CONGRATULATIONS"
msgstr ""

#. Type: note
#. Description
#: ../templates:15001
msgid "OpenEMR has been successfully installed."
msgstr ""

#. Type: note
#. Description
#: ../templates:15001
msgid "You can now set up OpenEMR by browsing to: http://localhost/openemr"
msgstr ""

#. Type: note
#. Description
#: ../templates:16001
msgid "OpenEMR has been successfully installed and configured."
msgstr ""

#. Type: note
#. Description
#: ../templates:16001
msgid "You can now use OpenEMR by browsing to: http://localhost/openemr"
msgstr ""

#. Type: note
#. Description
#: ../templates:16001
msgid "(user is 'admin' and password is 'pass')"
msgstr ""

#. Type: note
#. Description
#: ../templates:17001
msgid "OpenEMR has been successfully upgraded."
msgstr ""

#. Type: note
#. Description
#: ../templates:17001
msgid ""
"Recommend setting optional configuration settings in /var/www/openemr/sites/"
"<sitename>/config.php  (We have renamed your old configuration file to "
"config.OLD) (We recommend you delete the config.OLD file when done)"
msgstr ""

#. Type: note
#. Description
#: ../templates:17001
msgid ""
"We have placed backup of your old OpenEMR in /tmp/openemr-tmp/ (We recommend "
"you copy this somewhere protected since it contains confidential patient "
"information)"
msgstr ""

#. Type: error
#. Description
#. Type: error
#. Description
#. Type: error
#. Description
#. Type: error
#. Description
#. Type: error
#. Description
#: ../templates:18001 ../templates:19001 ../templates:20001 ../templates:21001
#: ../templates:22001
msgid "ERROR"
msgstr ""

#. Type: error
#. Description
#: ../templates:18001
msgid ""
"Unable to remove the openemr package, because you did not provide the "
"correct MySQL root password."
msgstr ""

#. Type: error
#. Description
#: ../templates:18001
msgid ""
"Ensure you know the MySQL root password before attempting to remove this "
"package in the future."
msgstr ""

#. Type: error
#. Description
#: ../templates:19001
msgid ""
"Unable to install the openemr package, because an instance of OpenEMR "
"already exist at /var/www/openemr."
msgstr ""

#. Type: error
#. Description
#: ../templates:20001
msgid ""
"This package does not support upgrading your current version of OpenEMR."
msgstr ""

#. Type: error
#. Description
#: ../templates:21001
msgid ""
"This package can not upgrade OpenEMR, because OpenEMR is not installed at /"
"var/www/openemr"
msgstr ""

#. Type: error
#. Description
#: ../templates:22001
msgid ""
"This package can not upgrade OpenEMR, because unable to confirm presence of "
"a mysql credentials and/or database."
msgstr ""
